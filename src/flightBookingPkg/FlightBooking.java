package flightBookingPkg;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class FlightBooking {

	WebDriver driver;
	String chromePath;
	// WebDriverWait

	/* Function to open Browser */
	public void openBrowser() {
		chromePath = ".\\ChromeBrowserDriver\\chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", chromePath);
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get("https://www.orbitz.com/");
	}

	/* Function to Close Browser */
	public void closeBrowser() {
		driver.close();
	}

	/* Test Case#1 - To Book Flight RoundTrip */
	public void bookFlightRoundTrip() throws InterruptedException, IOException {
		driver.findElement(By.id("tab-flight-tab")).click();

		/* Select From City = Pune */
		driver.findElement(By.id("flight-origin")).click();
		driver.findElement(By.id("flight-origin")).sendKeys("pune");
		Thread.sleep(2000);
		driver.findElement(By.id("aria-option-0")).click();

		/* Select Destination City = Mumbai */
		driver.findElement(By.id("flight-destination")).click();
		driver.findElement(By.id("flight-destination")).sendKeys("Mumbai");
		Thread.sleep(2000);
		driver.findElement(By.id("aria-option-0")).click();

		/* Select Departure Date */
		driver.findElement(By.id("flight-departing")).click();

		/* Effective way to pick active date from the calendar */
		List<WebElement> activeDeparttureDateList = driver.findElements
				(By.xpath("//div[@id='flight-departing-wrapper']/div/div/div[2]/"
						+ "table/tbody//tr//td/button[@class != 'datepicker-cal-date disabled']"));

		/* Click on first active date from the list */
		activeDeparttureDateList.get(0).click();

		/* Select Return Date */
		driver.findElement(By.id("flight-returning")).click();

		/* Another effective way to pick active date from the calendar */
		List<WebElement> activeReturnDateList = driver.findElements(By.xpath(
				"//div[@id='flight-returning-wrapper']/div/div/div[3]/table/tbody//tr//td/button[@class != 'datepicker-cal-date disabled']"));
		activeReturnDateList.get(2).click();

		/* Take a Screen Shot Before Clicking on Search Button */
		System.out.println("Page Title Before Clicking Search Button is..." + driver.getTitle());
		takeScreenShot(driver, ".\\ScreenShots\\BeforeSearch.png");

		/* Click on Search Button */
		driver.findElement(By.id("search-button")).click();

		/* Take a Screen Shot After Clicking on Search Button */
		System.out.println("Page Title After Clicking Search Button is..." + driver.getTitle());
		takeScreenShot(driver, ".\\ScreenShots\\AfterSearch.png");

	}

	/* Test Case# 2 -- Checking all links and clicking functionality */
	public void verifyHomePageLinks() throws IOException {
		// String [] homePageLinksArr={"Home\ncurrently
		// selected","Hotels","Flights","Packages","Cars","Vacation
		// Rentals","Cruises","Deals","Activities","Discover","Mobile","Rewards"};
		String[] homePageLinksArr = { "Home", "Hotels", "Flights", "Packages", "Cars", "Vacation Rentals", "Cruises",
				"Deals", "Activities", "Discover", "Mobile", "Rewards" };
		List<WebElement> homepageLinkList = driver.findElements(By.cssSelector(
				"ul[class='utility-nav nav-group cf'] >li[id*='-header']>a:not([id='primary-header-cruiseLegacy'])"));
		System.out.println("Total Links are : " + homepageLinkList.size());

		/*
		 * for Chrome browser one invisible link appears in the List Collection
		 * "homepageLinkList" As in the loop we are clicking each link, due to
		 * click on that invisible link click causes IndexoufOfBound Exception
		 * To Overcome this added filter
		 * a:not([id='primary-header-cruiseLegacy'] to CssSelector locator
		 */
		for (int i = 0; i < homepageLinkList.size(); i++) {
			homepageLinkList = driver.findElements(By.cssSelector(
					"ul[class='utility-nav nav-group cf'] >li[id*='-header']>a:not([id='primary-header-cruiseLegacy'])"));
			// System.out.println(homepageLinkList.get(i).getText());
			Assert.assertEquals("Link text mismatches", homePageLinksArr[i], homepageLinkList.get(i).getText());
			String pagename = homepageLinkList.get(i).getText() + ".png";
			homepageLinkList.get(i).click();
			takeScreenShot(driver, ".\\HomePageLinksScreenShots\\" + pagename);
		}

	}

	/*
	 * Test Case# 3 -- Checking On Selecting Child number respective Age
	 * dropdown appears or not
	 */
	public void verifyFlightPageChildrenDropDown() {
		driver.findElement(By.id("header-logo")).click();
		driver.findElement(By.id("tab-flight-tab")).click();
		WebElement childrenDropdown = driver.findElement(By.id("flight-children"));
		Select childrenDropdownSelectObj = new Select(childrenDropdown);
		/* Click on 6 from the drop down to appear new 6 age related dropdown */
		childrenDropdownSelectObj.selectByValue("6");
		List<WebElement> childAgeDropdownList = driver
				.findElements(By.cssSelector("label[id^='flight-age-select-']>Select"));
		// System.out.println("Age Dropdown Count is
		// "+childAgeDropdownList.size());
		Assert.assertEquals("Age Dropdown Count is not matching", 6, childAgeDropdownList.size());

		/* Checking all 6 dropdowns values, like count, default selected */
		Select childrenAgeDropdownSelectObj = null;
		for (int i = 0; i < childAgeDropdownList.size(); i++) {
			childAgeDropdownList.get(i).click();
			childrenAgeDropdownSelectObj = new Select(childAgeDropdownList.get(i));
			// System.out.println(childrenAgeDropdownSelectObj.getFirstSelectedOption().getText());
			Assert.assertEquals("Text mismatches", " Age ",
					childrenAgeDropdownSelectObj.getFirstSelectedOption().getText());
			// System.out.println("Count"
			// +childrenAgeDropdownSelectObj.getOptions().size() );
			Assert.assertEquals("DropDown values count mismatch", 19, childrenAgeDropdownSelectObj.getOptions().size());
		}
		/* Enabling radio button to be appear to webpage */
		childrenAgeDropdownSelectObj.selectByIndex(1);

		/* Validating Radio Button */
		WebElement inLapRadioButton = driver.findElement(By.id("flight-children-in-lap"));
		Assert.assertTrue("Radio Button In Lap is not selected By default", inLapRadioButton.isSelected());
		driver.findElement(By.id("flight-children-in-seat")).click();

		/*
		 * Here While Assert Execution Stale Element Exception occurring To
		 * overcome I reinitialize same webElement "inLapRadioButton" again
		 */
		inLapRadioButton = driver.findElement(By.id("flight-children-in-lap"));
		Assert.assertFalse("Radio Button In Lap is remains selected", inLapRadioButton.isSelected());
	}

	/* Test Case# 4 -- Handling multiple Windows */
	public void airlineAgeRuleLinkWindowHandle() {
		driver.findElement(By.id("flight-children-help")).click();
		Set<String> allWindowHandles = driver.getWindowHandles();
		Iterator<String> windowHandleIterator = allWindowHandles.iterator();
		System.out.println("Opened Window Count is " + allWindowHandles.size());
		String homePageId = null, airlineAgerulePageId = null;
		while (windowHandleIterator.hasNext()) {
			homePageId = (String) windowHandleIterator.next();
			windowHandleIterator.hasNext();
			airlineAgerulePageId = (String) windowHandleIterator.next();
		}
		driver.switchTo().window(airlineAgerulePageId);
		List<WebElement> ageRulePageLinks = driver.findElements(By.cssSelector("div[class='col']>ul>li>a"));
		System.out.println("Total Links on Airline Rule Page " + ageRulePageLinks.size());
		driver.close();
		driver.switchTo().window(homePageId);
	}

	public static void takeScreenShot(WebDriver driver, String filepath) throws IOException {
		TakesScreenshot scrShot = (TakesScreenshot) driver;
		File scrFile = scrShot.getScreenshotAs(OutputType.FILE);
		File desFile = new File(filepath);
		FileUtils.copyFile(scrFile, desFile);
	}

	public static void main(String[] args) throws InterruptedException, IOException {
		FlightBooking flightBookingObject = new FlightBooking();
		flightBookingObject.openBrowser();
		flightBookingObject.bookFlightRoundTrip();
		flightBookingObject.verifyHomePageLinks();
		flightBookingObject.verifyFlightPageChildrenDropDown();
		flightBookingObject.airlineAgeRuleLinkWindowHandle();
		flightBookingObject.closeBrowser();

	}

}
